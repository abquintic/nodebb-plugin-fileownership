<form class="form">
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<div class="form-group">
				<label>Filelist on User Profile</label>
				<input id="fileownership_userprofile" type="checkbox" {settings.fileownership_userprofile}>
			</div>
			<div class="form-group">
				<label>Unique filenames (per user)</label>
				<input id="fileownership_uniquefiles" type="checkbox" {settings.fileownership_uniquefiles}>
			</div>
		</div>
	</div>
</form>

<button class="btn btn-primary" id="save">Save</button>

<input id="csrf_token" type="hidden" value="{csrf}" />

<script type="text/javascript">


	$('#save').on('click', function() {
		var data = {
			_csrf: $('#csrf_token').val(),
			fileownership_userprofile: $('#fileownership_userprofile').is(":checked"),
			fileownership_uniquefiles: $('#fileownership_uniquefiles').is(":checked")
		};

		console.log(data);

		$.post(config.relative_path + '/api/admin/plugins/fileownership/save', data, function(data) {
			app.alertSuccess(data.message);
		});

		return false;
	});

</script>
