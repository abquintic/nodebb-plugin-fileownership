# nodebb-plugin-fileownership

This project is meant to be a very simple file ownership tracking system.
It collects very simple metadata about a file.

## What it does

The plugin stores a key per user at nodebb-plugin-fileownership:[uid]

The resulting object has:

  id: uid
  filelist: Array of metadata

  The metadata object is:
   fileName: Original name of the file
   storagePath: Where does the file live ( with name )
   size: Filesize
   date: Upload date stamp
   url: relative URL to the file
   type: mime type as provided by upload

## Configuration

  This plugin currently has two boolean configuration values.

## Hooks

  This currently fires on the topic.post and topic.reply action hooks. It fires the fileownershipProcessedFile action hook when it completes. It passes an array the metadata objects as processedFiles.

### Show on user profile

  This currently has no effect. Once working, this will show the user's uploaded files on their profile page.

### Unique Filenames (per user)

  This setting means the file tracking system will assume that a file uploaded again or a filename use use should replace the existing file. This does not currently prune the previously uploaded file.


## Other Notes

Logging is a little noisy right now.

## Updates

Confirmed to work with NodeBB v1.10.0
