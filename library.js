const winston = module.parent.require('winston');
const Meta = module.parent.require('./meta');
const async = require.main.require('async');
const db = module.parent.require('./database');

const utils = module.parent.require('./utils');
const nconf = module.parent.require('nconf');
const path = module.parent.require('path');
const fs = module.parent.require('fs');
const plugins = module.parent.require('./plugins');
const util = module.parent.require('util');


(function (fileownership) {
  const dbSettingsKey = 'nodebb-plugin-fileownership';

  fileownership.init = function (params, callback) {
    winston.verbose('FileOwnership - init.');
    params.router.get('/admin/plugins/fileownership', params.middleware.applyCSRF, params.middleware.admin.buildHeader, renderAdmin);
    params.router.get('/api/admin/plugins/fileownership', params.middleware.applyCSRF, renderAdmin);

    params.router.post('/api/admin/plugins/fileownership/save', params.middleware.applyCSRF, save);
    callback();
  };

  fileownership.stageToUploadQueue = function (data, callback)
    // Parse the storage info and place it in a holding queue.
    // This will get processed by a back end function that will be handled by other filters.
  {
    const uploadedFile = data.uploadedFile; // The file portion of the post as used by the uploadedFile filter.
																						 // {
																						 //   "fieldName":"files[]",
																						 //   "originalFilename":"example.sculpt",
																						 //   "path":"/tmp/3YHgPaBDrun7Ipsz5s39QZPF.sculpt",
																						 //   "headers":{"content-disposition":"form-data; name=\"files[]\"; filename=\"example.sculpt\"","content-type":"application/octet-stream"},
																						 //   "size":194457,
																						 //   "name":"example.sculpt",
																						 //   "type":"application/octet-stream"
																					   // }
    const storedFile = data.storedFile; // Where the file was *actually* saved and relevant, related data.
																						 // {
																						 //   "uid": 1,
																						 //   "url":"/assets/uploads/files/1498697674801-example.sculpt",
																						 //   "path":"/var/www/nodebb/public/uploads/files/1498697674801-example.sculpt",
																						 //   "name":"example.sculpt"
																					   // }

    winston.debug('[fileownership.stageToUploadQueue] Starting.' + JSON.stringify(data));
    let settings;

    // Make sure we have things.
    if (storedFile.path.length > 0) {
      const newFileObject = {};
      async.waterfall([
        function (next) {
          db.getObject(dbSettingsKey, next);
        },
        function (_settings, next) {
          settings = _settings || {};
          if (Date.now() >= settings.expiresAt) {
            refreshToken(next);
          } else {
            next();
          }
        },
        function (next) {
          newFileObject.fileName = uploadedFile.originalFilename;
          newFileObject.url = storedFile.url;
          newFileObject.storagePath = storedFile.path;
          newFileObject.date = Date.now();
          newFileObject.size = uploadedFile.size;
          newFileObject.type = uploadedFile.type;
	  doDBInsert(newFileObject, data.uid, settings, next);
        },
        function (next) {
          winston.debug(`[fileownership.stageToUploadQueue] Added ${newFileObject.fileName} to database.`);
          callback(null, data);
        },
      ], null);
    } else {
      {
        winston.error('[fileownership.stageToUploadQueue] A valid path was not provided!');
      }
    }
  };

  function doDBInsert(newFileObject, uid, settings, callback) {

    async.waterfall([
      function (next) {
        // Pull up any files already owned by the user.
        db.getObject(`${dbSettingsKey}:${uid}`, next);
      },
      function (_userFiles, next) {
        // Do things if there are results.
        let objectsLoop;

        let matchingPosition = -1;

				if ( _userFiles === null )
				{
					_userFiles = {};
					_userFiles.filelist = [];
					_userFiles.filelist.push(newFileObject);
					winston.debug(`[doDBInsert] Adding initial file to ${dbSettingsKey}:${uid}`);
				}
				else
				{
					if(_userFiles.filelist.length > 0)
					{
		        for (let queueLoop = 0; queueLoop < _userFiles.filelist.length; queueLoop++) {
		          if (_userFiles.filelist[queueLoop].fileName == newFileObject.fileName) {
		            matchingPosition = queueLoop;
		          }
		        }
					}

	        if (matchingPosition > -1) {
            if (!settings.unique) {
              // Replace with the new entry.
              winston.debug(`[doDBInsert] Replacing the existing entry on ${dbSettingsKey}:${uid}`);
              _userFiles.filelist[matchingPosition] = newFileObject;
            } else {
							winston.debug(`[doDBInsert] Adding new, non-unique file to ${dbSettingsKey}:${uid}`);
						  _userFiles.filelist.push(newFileObject);
						}
					} else {
            winston.debug(`[doDBInsert] Adding new, unique file to ${dbSettingsKey}:${uid}`);
            _userFiles.filelist.push(newFileObject);
          }
				}
			  next(null, _userFiles);
      },
      function (_writeArray, next) {
        db.setObject(`${dbSettingsKey}:${uid}`, _writeArray, next);
      },
      function (next) {
				if(plugins.hasListeners('action:fileownershipStoredFile'))
				{
          plugins.fireHook('action:fileownershipStoredFile', { processedFiles: newFileObject });
				}
			  callback(null);
      },
    ], null);
  }

  function renderAdmin(req, res, next) {
    db.getObject(dbSettingsKey, (err, settings) => {
      if (err) {
        return next(err);
      }
      settings = settings || {};
      const data = {
        fileownership_userprofile: settings.fileownership_userprofile == 'true' ? 'checked' : '',
        fileownership_uniquefiles: settings.fileownership_uniquefiles == 'true' ? 'checked' : '',
      };
      res.render('admin/plugins/fileownership', { settings: data, csrf: req.csrfToken() });
    });
  }

  function save(req, res, next) {
    const data = {
      fileownership_userprofile: req.body.fileownership_userprofile,
      fileownership_uniquefiles: req.body.fileownership_uniquefiles,
    };

    db.setObject(dbSettingsKey, data, (err) => {
      if (err) {
        return next(err);
      }

      res.status(200).json({ message: 'Settings saved!' });
    });
  }


  const admin = {};

  admin.menu = function (menu, callback) {
    menu.plugins.push({
      route: '/plugins/fileownership',
      icon: 'fa-cloud-upload',
      name: 'fileownership',
    });

    callback(null, menu);
  };


  fileownership.admin = admin;
}(module.exports));
